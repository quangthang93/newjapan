<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/variables.php'; ?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Title</title>
  <meta name="description" content="">
  <meta property="og:site_name" content="">
  <meta property="og:title" content="">
  <meta property="og:description" content="">
  <meta property="og:type" content="website">
  <meta property="og:url" content="">
  <meta property="og:image" content="">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="">
  <meta name="twitter:image" content="">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Libre+Baskerville:ital,wght@0,400;0,700;1,400&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo $PATH;?>/assets/css/index.css">
</head>

<body>
  <div class="wrapper">
    <header class="header header-thang">
      <div class="header-top">
        <h1 class="logo">
          <img src="<?php echo $PATH;?>/assets/images/common/h_logo.svg" alt="">
        </h1>
        <div class="header-action">
          <div class="sub header-action--tel">
            <div class="header-action--tel-label">TEL</div>
            <div class="header-action--tel-cnt">
              <p class="header-action--tel-num"><a href="tel:03-5610-3815">03-5610-3815</a></p>
              <p class="header-action--tel-text">［受付時間］平日・土曜 10:00~15:00</p>
            </div>
          </div>
          <div class="sub header-action--icon access"><a href="/about/access">アクセス</a></div>
          <div class="sub header-action--icon shop"><a href="/shop">
          ショップ</a></div>
          <div class="sub header-action--icon ticket"><a href="https://yyk1.ka-ruku.com/njp-s/showList" target="_blank">チケット購入</a></div>
        </div>
      </div><!-- ./header-top -->
      <div class="header-nav js-menu">
        <div class="top-thang type2 sp-only">
          <div class="main-sidebar--search">
            <form action="">
              <input class="input" type="text" name="q" placeholder="サイト内検索">
              <input class="submit" type="submit" value="">
            </form>
          </div><!-- ./main-sidebar--search -->
        </div>
        <div class="sub header-action--tel sp-only">
          <div class="header-action--tel-label">TEL</div>
          <div class="header-action--tel-cnt">
            <p class="header-action--tel-num"><a href="tel:03-5610-3815">03-5610-3815</a></p>
            <p class="header-action--tel-text">［受付時間］平日・土曜 10:00~15:00</p>
          </div>
        </div>
        <nav>
          <ul>
            <li class="sp-only"><a href="">トップ</a></li>
            <li><a href="">公演情報</a></li>
            <li><a href="">チケット購入について</a></li>
            <li><a href="">新日本フィルについて</a></li>
            <li><a href="">活動の支援</a></li>
            <li><a href="">動画ライブラリー</a></li>
          </ul>
          <div class="sub header-action--icon ticket sp-only"><a href="https://yyk1.ka-ruku.com/njp-s/showList" target="_blank">チケット購入</a></div>
        </nav>
        <div class="header-nav--search pc-only">
          <div class="header-nav--search-btn js-search-btn"></div>
          <form class="js-search-form" action="/search">
            <input type="hidden" name="cx" value="003476608630538492024:_vmv3q5_hjg">
            <input type="hidden" name="ie" value="UTF-8">
            <input type="text" name="q" class="js-search-boxInput search-boxInput">
            <input type="submit" value="検索">
          </form>
        </div>
      </div>
      <div class="header-ctrl sp-only">
        <a class="header-ctrl--btn js-menuTrigger" href="javascript:void(0);">
          <span></span>
          <span></span>
          <span></span>
        </a>
        <p class="header-ctrl--label js-menuLabel">MENU</p>
      </div>
      <div class="header-tel sp-only">
        <a class="header-tel--num" href="tel:03-5610-3815">
          <img src="<?php echo $PATH;?>/assets/images/common/icon-phone.svg" alt="">
        </a>
        <p class="header-tel--label">TEL</p>
      </div>
      <div class="header-line sp-only"></div>
    <div class="sub header-action--icon ticket type2 sp-only"><a href="https://yyk1.ka-ruku.com/njp-s/showList" target="_blank">チケット購入</a></div>
    </header><!-- ./header -->