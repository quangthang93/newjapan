    <footer class="footer footer-thang">
      <div class="footer-topWrap">
        <div class="footer-logo">
          <img src="<?php echo $PATH;?>/assets/images/common/f_logo.svg" alt="">
        </div>
        <div class="footer-top">
          <div class="footer-nav">
            <div class="footer-nav--item">
              <div class="footer-nav--direct"><a href="">公演情報</a></div>
              <ul class="footer-nav--links">
                <li><a href="">定期演奏会</a></li>
                <li><a href="">特別演奏会</a></li>
                <li><a href="">室内楽シリーズ</a></li>
                <li><a href="">その他演奏会</a></li>
                <li><a href="">アーカイブ</a></li>
              </ul>
            </div>
            <div class="footer-nav--item">
              <div class="footer-nav--direct"><a href="">チケット購入について</a></div>
              <ul class="footer-nav--links">
                <li><a href="">連続券</a></li>
                <li><a href="">チケット・マイプラン</a></li>
                <li><a href="">1回券・学生券・学生回数券</a></li>
                <li><a href="">メンバーズカード</a></li>
              </ul>
            </div>
            <div class="footer-nav--item">
              <div class="footer-nav--direct"><a href="">新日本フィルについて</a></div>
              <ul class="footer-nav--links">
                <li><a href="">指揮者紹介</a></li>
                <li><a href="">楽団員一覧</a></li>
                <li><a href="">新日本フィル・ワールド・ドリーム・オーケストラ</a></li>
                <li><a href="">アクセスと周辺情報</a></li>
                <li><a href="">財団概要・事業運営</a></li>
                <li><a href="">教育・社会貢献</a></li>
              </ul>
            </div>
            <div class="footer-nav--item">
              <div class="footer-nav--direct"><a href="">活動の支援</a></div>
            </div>
            <div class="footer-nav--item">
              <div class="footer-nav--direct"><a href="">動画ライブラリー</a></div>
            </div>
            <div class="footer-nav--item">
              <div class="footer-nav--direct"><a href="">新日本フィルMAGAZINE</a></div>
            </div>
            <div class="footer-nav--item">
              <div class="footer-nav--direct"><a href="">オーディション・募集</a></div>
            </div>
            <div class="footer-nav--item">
              <div class="footer-nav--direct"><a href="">オンラインショッピング</a></div>
            </div>
            <div class="footer-nav--item">
              <div class="footer-nav--direct"><a href="">最新情報</a></div>
            </div>
          </div><!-- ./footer-nav -->
          <div class="footer-sns">
            <a href="" class="footer-sns--item"><img src="<?php echo $PATH;?>/assets/images/common/icon-facebook.svg" alt=""></a>
            <a href="" class="footer-sns--item"><img src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt=""></a>
            <a href="" class="footer-sns--item"><img src="<?php echo $PATH;?>/assets/images/common/icon-youtube.svg" alt=""></a>
            <a href="" class="footer-sns--item"><img src="<?php echo $PATH;?>/assets/images/common/icon-insta.svg" alt=""></a>
          </div>
        </div><!-- ./footer-top -->
      </div><!-- ./footer-topWrap -->
      <div class="footer-copy">
        <ul class="footer-copy--nav">
          <li><a href="">特定商取引法に基づく表示</a></li>
          <li><a href="">プライバシーポリシー</a></li>
          <li><a href="">お問い合わせ</a></li>
        </ul>
        <p class="footer-copy--infor">
          © 2021 New Japan Philharmonic, All rights reserved.
        </p>
      </div><!-- ./footer-copy -->
    </footer><!-- ./footer -->
  </div>
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery-3.5.1.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/ofi.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/slick.min.js"></script>
  <!-- <script src="<?php echo $PATH;?>/assets/js/libs/jquery.matchHeight-min.js"></script> -->
  <script src="<?php echo $PATH;?>/assets/js/common.js"></script>
  <!-- <script src="<?php echo $PATH;?>/assets/js/top.js"></script> -->
</body>

</html>