$(function() {

  /* --------------------
  GLOBAL VARIABLE
  --------------------- */
  // Selector
  // var $pageTop = $('.js-pageTop');

  // Init Value
  var breakpointSP = 767,
      breakpointTB = 1050,
      wWindow = $(window).outerWidth();


  /* --------------------
  FUNCTION COMMON
  --------------------- */
  // Menu trigger
  var triggerMenu = function() {
    $('.js-menuTrigger').click(function() {
      $('body').toggleClass('fixed');
      $(this).toggleClass('open');
      $('.js-menu').toggleClass('open');
      var $menuLabel = $('.js-menuLabel').html();
      $('.js-menuLabel').html(($menuLabel === "MENU") ? "CLOSE" : "MENU");
    });
  }

  // Slider init
  // Slider (Restaurant detail page)
  var initSliderRest = function() {
    var $slide01 = $('.js-mainSlider'); 

    // Slider 01
    if ($slide01.length) {
      $slide01.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        swipeToSlide: true,
        // fade: true,
        prevArrow: '<a class="slick-prev" href="#"><span></span></a>',
        nextArrow: '<a class="slick-next" href="#"><span></span></a>',
        // responsive: [{
        //   breakpoint: 768,
        //   settings: {
        //     slidesToShow: 1
        //   }
        // }]
      });
    }
  }


  var triggerFormSearch = function () {
    $('.js-search-btn').click(function() {
      $('.js-search-form').toggleClass('active');
    }); 
  }

  var triggerNewsBox = function () {
    $('.js-has-news').click(function() {
      $('.js-boxNewsClose').show();
      $('.js-has-news').removeClass('active');
      $(this).addClass('active');
      $('.js-news-box').toggle();
    }); 

    $('.js-boxNewsClose').click(function() {
      $(this).hide();
      $('.js-news-box').hide();
    });
  }

  /* --------------------
  INIT (WINDOW ON LOAD)
  --------------------- */
  // Run all script when DOM has loaded
  var init = function() {
    // objectFitImages();
    initSliderRest();
    triggerMenu();
    triggerFormSearch();
    triggerNewsBox();
  }

  init();


  /* --------------------
  WINDOW ON RESIZE
  --------------------- */
  // $(window).resize(function() {
  //   wWindow = $(window).outerWidth();
  // });


  /* --------------------
  WINDOW ON SCROLL
  --------------------- */
  // $(window).scroll(function() {
  //   var scroll = $(this).scrollTop();
  //   $('.fadeup').each(function() {
  //     var elemPos = $(this).offset().top;
  //     var windowHeight = $(window).height();
  //     if (scroll > elemPos - windowHeight + 100) {
  //       $(this).addClass('in');
  //     }
  //   });
  // });

});