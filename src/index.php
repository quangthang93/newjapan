<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main top-thang">
  <div class="main-inner">
    <div class="main-sidebar pc-only">
      <div class="main-sidebar--calendar">
        <p class="main-sidebar--calendar-ttl">EVENT<span>イベントカレンダー</span></p>
        <div class="customcalendar">
          <div class="widget_calendar">
            <div class="">
              <table id="wp-calendar" class="js-wp-calendar">
                <caption>
                  <div class="date">
                    2020<span class="month">12</span>Dec.
                  </div>
                  <a href="javascript:void(0)" class="ctrl prev"></a>
                  <a href="javascript:void(0)" class="ctrl next"></a>
                </caption>
                <thead>
                  <tr>
                    <th title="月曜日" scope="col">Mon.</th>
                    <th title="火曜日" scope="col">Tue.</th>
                    <th title="水曜日" scope="col">Wed.</th>
                    <th title="木曜日" scope="col">Thu.</th>
                    <th title="金曜日" scope="col">Fri.</th>
                    <th title="土曜日" scope="col">Sat.</th>
                    <th title="日曜日" scope="col">Sun.</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><a href="javascript:void(0)">1</a></td>
                    <td><a class="js-has-news blue" href="javascript:void(0)">2</a></td>
                    <td><a href="javascript:void(0)">3</a></td>
                    <td><a class="js-has-news blue" href="javascript:void(0)">4</a></td>
                    <td><a href="javascript:void(0)">5</a></td>
                    <td><a href="javascript:void(0)">6</a></td>
                    <td><a href="javascript:void(0)">7</a></td>
                  </tr>
                  <tr>
                    <td><a class="js-has-news blue active" href="javascript:void(0)">8</a></td>
                    <td><a href="javascript:void(0)">9</a></td>
                    <td><a href="javascript:void(0)">10</a></td>
                    <td><a href="javascript:void(0)">11</a></td>
                    <td><a href="javascript:void(0)">12</a></td>
                    <td><a href="javascript:void(0)">13</a></td>
                    <td><a class="js-has-news orange" href="javascript:void(0)">14</a></td>
                  </tr>
                  <tr>
                    <td><a href="javascript:void(0)">15</a></td>
                    <td><a href="javascript:void(0)">16</a></td>
                    <td><a href="javascript:void(0)">17</a></td>
                    <td><a href="javascript:void(0)">18</a></td>
                    <td><a class="js-has-news blue" href="javascript:void(0)">19</a></td>
                    <td><a class="js-has-news green" href="javascript:void(0)">20</a></td>
                    <td><a href="javascript:void(0)">21</a></td>
                  </tr>
                  <tr>
                    <td><a href="javascript:void(0)">22</a></td>
                    <td><a class="js-has-news blue" href="javascript:void(0)">23</a></td>
                    <td><a class="js-has-news blue" href="javascript:void(0)">24</a></td>
                    <td><a href="javascript:void(0)">25</a></td>
                    <td><a href="javascript:void(0)">26</a></td>
                    <td><a href="javascript:void(0)">27</a></td>
                    <td><a class="js-has-news green" href="javascript:void(0)">28</a></td>
                  </tr>
                  <tr>
                    <td><a class="js-has-news blue" href="javascript:void(0)">29</a></td>
                    <td><a href="javascript:void(0)">30</a></td>
                    <td><a class="js-has-news blue" href="javascript:void(0)">31</a></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
                <tfoot>
                  <tr>
                    <td colsan="3">
                      <a title="先月" href="/concerts?search_date_from=2020-12-01">« 12月</a>
                    </td>
                    <td class="pad">&nbsp;</td>
                    <td colspan="3">
                      <a title="翌月" href="/concerts?search_date_from=2021-02-01"> 2月 » </a>
                    </td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div><!-- ./widget_calendar -->
          <div class="boxNews js-news-box">
            <a href="">
              <p class="boxNews-ttl">10月8日開催の公演</p>
              <div class="boxNews-cnt">
                <p class="boxNews-cnt--subject">定期演奏会 | JADE</p>
                <div class="boxNews-cnt--main">
                  <div class="boxNews-cnt--main-thumb">
                    <img src="<?php echo $PATH;?>/assets/images/top/boxnews01.jpg" alt="">
                  </div>
                  <div class="boxNews-cnt--main-infor">
                    <p class="boxNews-cnt--main-date">2020.10.08 THU 19:00~</p>
                    <p class="boxNews-cnt--main-des">#625 ェイド〈サントリーホール・シリーズ〉</p>
                  </div>
                </div>
                <p class="boxNews-cnt--des">ブラームス：ヴァイオリン協奏曲 ニ長調 op.77、チャイコフスキー：交響曲第４番 へ短調 op.36</p>
                <p class="main-featured--item-local">サントリーホール</p>
              </div>
            </a>
          </div><!-- ./boxNews -->
          <a href="javascript:void(0)" class="boxNews-close js-boxNewsClose"></a>
        </div><!-- ./customcalendar -->
      </div><!-- ./main-sidebar--calendar -->
      <div class="main-sidebar--advers">
        <div class="main-adversWrap">
          <a href="" class="main-advers">
            <img src="<?php echo $PATH;?>/assets/images/top/adver01.png" alt="">
          </a>
          <a href="" class="main-advers">
            <img src="<?php echo $PATH;?>/assets/images/top/adver02.png" alt="">
          </a>
          <a href="" class="main-advers">
            <img src="<?php echo $PATH;?>/assets/images/top/adver03.png" alt="">
          </a>
          <a href="" class="main-advers">
            <img src="<?php echo $PATH;?>/assets/images/top/adver04.png" alt="">
          </a>
          <a href="" class="main-advers">
            <img src="<?php echo $PATH;?>/assets/images/top/adver05.png" alt="">
          </a>
          <a href="" class="main-advers">
            <img src="<?php echo $PATH;?>/assets/images/top/adver06.png" alt="">
          </a>
        </div>
      </div><!-- ./main-sidebar--advers -->
      <div class="main-sidebar--search">
        <form action="">
          <input class="input" type="text" name="q" placeholder="サイト内検索">
          <input class="submit" type="submit" value="">
        </form>
      </div><!-- ./main-sidebar--search -->
      <ul class="main-sidebar--nav">
        <li><a href="">特定商取引法に基づく表示</a></li>
        <li><a href="">プライバシーポリシー</a></li>
        <li><a href="">お問い合わせ</a></li>
      </ul><!-- ./main-sidebar--nav -->
    </div><!-- ./main-sidebar -->
    <div class="main-content">
      <div class="main-sliderWrap">
        <ul class="main-slider js-mainSlider">
          <li>
            <a class="link" href="">
              <div class="main-slider--thumb">
                <img src="<?php echo $PATH;?>/assets/images/top/slider01.jpg" alt="">
              </div>
              <div class="main-inner--cntWrap">
                <div class="main-slider--cnt">
                  <div class="main-slider--cnt-label">
                    <div class="main-slider--cnt-label-img">
                      <img src="<?php echo $PATH;?>/assets/images/common/icon-trumpet.svg" alt="">
                    </div>
                    <p class="main-slider--cnt-label-txt">公演情報</p>
                  </div>
                  <div class="main-slider--cnt-box">
                    <h3 class="main-slider--cnt-ttl">新鋭・熊倉優が新日本フィルの定期演奏会に初登場！10/8（木）ジェイド 〈サントリーホール・シリーズ〉</h3>
                    <p class="main-slider--cnt-des">ブラームス：ヴァイオリン協奏曲 、チャイコフスキー：交響曲第4番（指揮：熊倉優、ヴァイオリン：竹澤恭子）</p>
                  </div>
                </div>
              </div>
            </a>
          </li>
          <li>
            <a class="link" href="">
              <div class="main-slider--thumb">
                <img src="<?php echo $PATH;?>/assets/images/top/slider01.jpg" alt="">
              </div>
              <div class="main-inner--cntWrap">
                <div class="main-slider--cnt">
                  <div class="main-slider--cnt-label">
                    <div class="main-slider--cnt-label-img">
                      <img src="<?php echo $PATH;?>/assets/images/common/icon-trumpet.svg" alt="">
                    </div>
                    <p class="main-slider--cnt-label-txt">公演情報</p>
                  </div>
                  <div class="main-slider--cnt-box">
                    <h3 class="main-slider--cnt-ttl">新鋭・熊倉優が新日本フィルの定期演奏会に初登場！10/8（木）ジェイド 〈サントリーホール・シリーズ〉</h3>
                    <p class="main-slider--cnt-des">ブラームス：ヴァイオリン協奏曲 、チャイコフスキー：交響曲第4番（指揮：熊倉優、ヴァイオリン：竹澤恭子）</p>
                  </div>
                </div>
              </div>
            </a>
          </li>
          <li>
            <a class="link" href="">
              <div class="main-slider--thumb">
                <img src="<?php echo $PATH;?>/assets/images/top/slider01.jpg" alt="">
              </div>
              <div class="main-inner--cntWrap">
                <div class="main-slider--cnt">
                  <div class="main-slider--cnt-label">
                    <div class="main-slider--cnt-label-img">
                      <img src="<?php echo $PATH;?>/assets/images/common/icon-trumpet.svg" alt="">
                    </div>
                    <p class="main-slider--cnt-label-txt">公演情報</p>
                  </div>
                  <div class="main-slider--cnt-box">
                    <h3 class="main-slider--cnt-ttl">新鋭・熊倉優が新日本フィルの定期演奏会に初登場！10/8（木）ジェイド 〈サントリーホール・シリーズ〉</h3>
                    <p class="main-slider--cnt-des">ブラームス：ヴァイオリン協奏曲 、チャイコフスキー：交響曲第4番（指揮：熊倉優、ヴァイオリン：竹澤恭子）</p>
                  </div>
                </div>
              </div>
            </a>
          </li>
        </ul>
      </div><!-- ./main-sliderWrap -->
      <div class="main-topic">
        <p class="main-topic--lb">重要なお知らせ</p>
        <a href="" class="main-topic--cnt">新型コロナウィルス対応に関する重要なお知らせ一覧（2020/9/29更新）</a>
      </div><!-- ./main-topic -->
      <div class="main-news sp-only">
        <h2 class="main-ttl">News<span>最新情報</span></h2>
        <ul class="main-news--list">
          <li class="main-news--item">
            <a href="">
              <p class="main-news--item-date">2020.09.30</p>
              <p class="main-news--item-des">10/17（土）ライブ配信決定！ルビー #34</p>
            </a>
          </li>
          <li class="main-news--item">
            <a href="">
              <p class="main-news--item-date">2020.09.30<span class="notice">重要なお知らせ</span></p>
              <p class="main-news--item-des">10/17（土）ライブ配信決定！ルビー #34</p>
            </a>
          </li>
          <li class="main-news--item">
            <a href="">
              <p class="main-news--item-date">2020.09.30</p>
              <p class="main-news--item-des">10/17（土）ライブ配信決定！ルビー #34</p>
            </a>
          </li>
          <li class="main-news--item">
            <a href="">
              <p class="main-news--item-date">2020.09.30</p>
              <p class="main-news--item-des">10/17（土）ライブ配信決定！ルビー #34</p>
            </a>
          </li>
        </ul>
        <div class="align-center"><a href="/news" class="main-news--viewmore">一覧を見る</a></div>
      </div><!-- ./main-news SP -->
      <div class="main-sidebar--calendar sp-only">
        <p class="main-sidebar--calendar-ttl">EVENT<span>イベントカレンダー</span></p>
        <div class="customcalendar">
          <div class="widget_calendar">
            <div class="">
              <table id="wp-calendar" class="js-wp-calendar">
                <caption>
                  <div class="date">
                    2020<span class="month">12</span>Dec.
                  </div>
                  <a href="javascript:void(0)" class="ctrl prev"></a>
                  <a href="javascript:void(0)" class="ctrl next"></a>
                </caption>
                <thead>
                  <tr>
                    <th title="月曜日" scope="col">Mon.</th>
                    <th title="火曜日" scope="col">Tue.</th>
                    <th title="水曜日" scope="col">Wed.</th>
                    <th title="木曜日" scope="col">Thu.</th>
                    <th title="金曜日" scope="col">Fri.</th>
                    <th title="土曜日" scope="col">Sat.</th>
                    <th title="日曜日" scope="col">Sun.</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><a href="javascript:void(0)">1</a></td>
                    <td><a class="js-has-news blue" href="javascript:void(0)">2</a></td>
                    <td><a href="javascript:void(0)">3</a></td>
                    <td><a class="js-has-news blue" href="javascript:void(0)">4</a></td>
                    <td><a href="javascript:void(0)">5</a></td>
                    <td><a href="javascript:void(0)">6</a></td>
                    <td><a href="javascript:void(0)">7</a></td>
                  </tr>
                  <tr>
                    <td><a class="js-has-news blue active" href="javascript:void(0)">8</a></td>
                    <td><a href="javascript:void(0)">9</a></td>
                    <td><a href="javascript:void(0)">10</a></td>
                    <td><a href="javascript:void(0)">11</a></td>
                    <td><a href="javascript:void(0)">12</a></td>
                    <td><a href="javascript:void(0)">13</a></td>
                    <td><a class="js-has-news orange" href="javascript:void(0)">14</a></td>
                  </tr>
                  <tr>
                    <td><a href="javascript:void(0)">15</a></td>
                    <td><a href="javascript:void(0)">16</a></td>
                    <td><a href="javascript:void(0)">17</a></td>
                    <td><a href="javascript:void(0)">18</a></td>
                    <td><a class="js-has-news blue" href="javascript:void(0)">19</a></td>
                    <td><a class="js-has-news green" href="javascript:void(0)">20</a></td>
                    <td><a href="javascript:void(0)">21</a></td>
                  </tr>
                  <tr>
                    <td><a href="javascript:void(0)">22</a></td>
                    <td><a class="js-has-news blue" href="javascript:void(0)">23</a></td>
                    <td><a class="js-has-news blue" href="javascript:void(0)">24</a></td>
                    <td><a href="javascript:void(0)">25</a></td>
                    <td><a href="javascript:void(0)">26</a></td>
                    <td><a href="javascript:void(0)">27</a></td>
                    <td><a class="js-has-news green" href="javascript:void(0)">28</a></td>
                  </tr>
                  <tr>
                    <td><a class="js-has-news blue" href="javascript:void(0)">29</a></td>
                    <td><a href="javascript:void(0)">30</a></td>
                    <td><a class="js-has-news blue" href="javascript:void(0)">31</a></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
                <tfoot>
                  <tr>
                    <td colsan="3">
                      <a title="先月" href="/concerts?search_date_from=2020-12-01">« 12月</a>
                    </td>
                    <td class="pad">&nbsp;</td>
                    <td colspan="3">
                      <a title="翌月" href="/concerts?search_date_from=2021-02-01"> 2月 » </a>
                    </td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div><!-- ./widget_calendar -->
          <div class="boxNews js-news-box">
            <a href="">
              <p class="boxNews-ttl">10月8日開催の公演</p>
              <div class="boxNews-cnt">
                <p class="boxNews-cnt--subject">定期演奏会 | JADE</p>
                <div class="boxNews-cnt--main">
                  <div class="boxNews-cnt--main-thumb">
                    <img src="<?php echo $PATH;?>/assets/images/top/boxnews01.jpg" alt="">
                  </div>
                  <div class="boxNews-cnt--main-infor">
                    <p class="boxNews-cnt--main-date">2020.10.08 THU 19:00~</p>
                    <p class="boxNews-cnt--main-des">#625 ェイド〈サントリーホール・シリーズ〉</p>
                  </div>
                </div>
                <p class="boxNews-cnt--des">ブラームス：ヴァイオリン協奏曲 ニ長調 op.77、チャイコフスキー：交響曲第４番 へ短調 op.36</p>
                <p class="main-featured--item-local">サントリーホール</p>
              </div>
            </a>
          </div><!-- ./boxNews -->
          <a href="javascript:void(0)" class="boxNews-close js-boxNewsClose"></a>
        </div><!-- ./customcalendar -->
      </div><!-- ./main-sidebar--calendar -->
      <div class="main-featured">
        <h2 class="main-ttl">Featured<span>注目公演</span></h2>
        <div class="main-featured--list">
          <div class="main-featured--item">
            <a href="">
              <div class="main-featured--item-thumb">
                <img src="<?php echo $PATH;?>/assets/images/top/featured01.jpg" alt="">
                <div class="main-featured--item-thumb-stamp" style="color: #911D2A">RUBY</div>
              </div>
              <div class="main-featured--item-cnt">
                <p class="main-featured--item-date">2020.10.08 THU 19:00~</p>
                <h3 class="main-featured--item-ttl">#625 ジェイド〈サントリーホール・シリーズ〉</h3>
                <p class="main-featured--item-des">ブラームス：ヴァイオリン協奏曲 ニ長調 op.77、チャイコフスキー：交響曲第４番 へ短調 op.36</p>
                <p class="main-featured--item-local">サントリーホール</p>
              </div>
            </a>
          </div>
          <div class="main-featured--item">
            <a href="">
              <div class="main-featured--item-thumb">
                <img src="<?php echo $PATH;?>/assets/images/top/featured02.jpg" alt="">
                <div class="main-featured--item-thumb-stamp" style="color: #927A2F">特別演奏会</div>
              </div>
              <div class="main-featured--item-cnt">
                <p class="main-featured--item-date">2020.10.08 THU 19:00~</p>
                <h3 class="main-featured--item-ttl">#625 ジェイド〈サントリーホール・シリーズ〉</h3>
                <p class="main-featured--item-des">ブラームス：ヴァイオリン協奏曲 ニ長調 op.77、チャイコフスキー：交響曲第４番 へ短調 op.36</p>
                <p class="main-featured--item-local">サントリーホール</p>
              </div>
            </a>
          </div>
          <div class="main-featured--item">
            <a href="">
              <div class="main-featured--item-thumb">
                <img src="<?php echo $PATH;?>/assets/images/top/featured03.jpg" alt="">
                <div class="main-featured--item-thumb-stamp" style="color: #399A27">JADE</div>
              </div>
              <div class="main-featured--item-cnt">
                <p class="main-featured--item-date">2020.10.08 THU 19:00~</p>
                <h3 class="main-featured--item-ttl">#625 ジェイド〈サントリーホール・シリーズ〉</h3>
                <p class="main-featured--item-des">ブラームス：ヴァイオリン協奏曲 ニ長調 op.77、チャイコフスキー：交響曲第４番 へ短調 op.36</p>
                <p class="main-featured--item-local">サントリーホール</p>
              </div>
            </a>
          </div>
          <div class="main-featured--item">
            <a href="">
              <div class="main-featured--item-thumb">
                <img src="<?php echo $PATH;?>/assets/images/top/featured04.jpg" alt="">
                <div class="main-featured--item-thumb-stamp" style="color: #142545">TOPAZ</div>
              </div>
              <div class="main-featured--item-cnt">
                <p class="main-featured--item-date">2020.10.08 THU 19:00~</p>
                <h3 class="main-featured--item-ttl">#625 ジェイド〈サントリーホール・シリーズ〉</h3>
                <p class="main-featured--item-des">ブラームス：ヴァイオリン協奏曲 ニ長調 op.77、チャイコフスキー：交響曲第４番 へ短調 op.36</p>
                <p class="main-featured--item-local">サントリーホール</p>
              </div>
            </a>
          </div>
          <div class="main-featured--item">
            <a href="">
              <div class="main-featured--item-thumb">
                <img src="<?php echo $PATH;?>/assets/images/top/featured05.jpg" alt="">
                <div class="main-featured--item-thumb-stamp" style="color: #927A2F">TOPAZ</div>
              </div>
              <div class="main-featured--item-cnt">
                <p class="main-featured--item-date">2020.10.08 THU 19:00~</p>
                <h3 class="main-featured--item-ttl">#625 ジェイド〈サントリーホール・シリーズ〉</h3>
                <p class="main-featured--item-des">ブラームス：ヴァイオリン協奏曲 ニ長調 op.77、チャイコフスキー：交響曲第４番 へ短調 op.36</p>
                <p class="main-featured--item-local">サントリーホール</p>
              </div>
            </a>
          </div>
          <div class="main-featured--item">
            <a href="">
              <div class="main-featured--item-thumb">
                <img src="<?php echo $PATH;?>/assets/images/top/featured06.jpg" alt="">
                <div class="main-featured--item-thumb-stamp" style="color: #911D2A">RUBY</div>
              </div>
              <div class="main-featured--item-cnt">
                <p class="main-featured--item-date">2020.10.08 THU 19:00~</p>
                <h3 class="main-featured--item-ttl">#625 ジェイド〈サントリーホール・シリーズ〉</h3>
                <p class="main-featured--item-des">ブラームス：ヴァイオリン協奏曲 ニ長調 op.77、チャイコフスキー：交響曲第４番 へ短調 op.36</p>
                <p class="main-featured--item-local">サントリーホール</p>
              </div>
            </a>
          </div>
        </div><!-- ./main-featured--list -->
      </div><!-- ./main-featured -->
      <div class="main-adversWrap sp-only">
        <a href="" class="main-advers">
          <img src="<?php echo $PATH;?>/assets/images/top/adver01.png" alt="">
        </a>
        <a href="" class="main-advers">
          <img src="<?php echo $PATH;?>/assets/images/top/adver02.png" alt="">
        </a>
        <a href="" class="main-advers">
          <img src="<?php echo $PATH;?>/assets/images/top/adver03.png" alt="">
        </a>
        <a href="" class="main-advers">
          <img src="<?php echo $PATH;?>/assets/images/top/adver04.png" alt="">
        </a>
        <a href="" class="main-advers">
          <img src="<?php echo $PATH;?>/assets/images/top/adver05.png" alt="">
        </a>
        <a href="" class="main-advers">
          <img src="<?php echo $PATH;?>/assets/images/top/adver06.png" alt="">
        </a>
      </div><!-- ./main-featured SP -->
      <div class="main-newsWrap">
        <div class="main-news pc-only">
          <h2 class="main-ttl type2">News<span>最新情報</span></h2>
          <ul class="main-news--list">
            <li class="main-news--item">
              <a href="">
                <p class="main-news--item-date">2020.09.30</p>
                <p class="main-news--item-des">10/17（土）ライブ配信決定！ルビー #34</p>
              </a>
            </li>
            <li class="main-news--item">
              <a href="">
                <p class="main-news--item-date">2020.09.30<span class="notice">重要なお知らせ</span></p>
                <p class="main-news--item-des">10/17（土）ライブ配信決定！ルビー #34</p>
              </a>
            </li>
            <li class="main-news--item">
              <a href="">
                <p class="main-news--item-date">2020.09.30</p>
                <p class="main-news--item-des">10/17（土）ライブ配信決定！ルビー #34</p>
              </a>
            </li>
            <li class="main-news--item">
              <a href="">
                <p class="main-news--item-date">2020.09.30</p>
                <p class="main-news--item-des">10/17（土）ライブ配信決定！ルビー #34</p>
              </a>
            </li>
          </ul>
          <a href="/news" class="main-news--viewmore">一覧を見る</a>
        </div><!-- ./main-news -->
        <div class="main-sns">
          <h2 class="main-ttl type2 pc-only">SNS<span>公式Twitterアカウント</span></h2>
          <h2 class="main-ttl sp-only">SNS<span>公式Twitterアカウント</span></h2>
          <div class="main-sns--iframe">
            <img src="<?php echo $PATH;?>/assets/images/common/twitter.jpg" alt="">
          </div>
          <div class="main-adversWrap">
            <a href="" class="main-advers">
              <img src="<?php echo $PATH;?>/assets/images/top/adver-news01.jpg" alt="">
              <div class="main-advers--infor">
                <p class="main-advers--ttl">年間プログラム</p>
                <p class="main-advers--time">[ 2021/2022シーズン ]</p>
              </div>
            </a>
            <a href="" class="main-advers">
              <img src="<?php echo $PATH;?>/assets/images/top/adver-news01.jpg" alt="">
              <div class="main-advers--infor">
                <p class="main-advers--ttl">年間プログラム</p>
                <p class="main-advers--time">[ 2021/2022シーズン ]</p>
              </div>
            </a>
          </div>
        </div><!-- ./main-sns -->
      </div><!-- ./main-newsWrap -->
      <div class="main-magazine">
        <div class="main-magazine--inner">
          <h2 class="main-ttl type3"><span>新日本フィル</span>Magazine</h2>
          <div class="main-magazine--list">
            <div class="main-magazine--item">
              <a href="">
                <img src="<?php echo $PATH;?>/assets/images/top/magazine01.jpg" alt="">
                <div class="main-magazine--item-infor">
                  <div class="main-magazine--item-inner">
                    <p class="main-magazine--item-tag"># 動画メッセージ</p>
                    <p class="main-magazine--item-des">ピアニスト 上原彩子氏よりメッセージ＃626トパーズ</p>
                  </div>
                </div>
              </a>
            </div>
            <div class="main-magazine--item">
              <a href="">
                <img src="<?php echo $PATH;?>/assets/images/top/magazine02.jpg" alt="">
                <div class="main-magazine--item-infor">
                  <div class="main-magazine--item-inner">
                    <p class="main-magazine--item-tag"># 動画メッセージ</p>
                    <p class="main-magazine--item-des">ピアニスト 上原彩子氏よりメッセージ＃626トパーズ</p>
                  </div>
                </div>
              </a>
            </div>
            <div class="main-magazine--item">
              <a href="">
                <img src="<?php echo $PATH;?>/assets/images/top/magazine03.jpg" alt="">
                <div class="main-magazine--item-infor">
                  <div class="main-magazine--item-inner">
                    <p class="main-magazine--item-tag"># 動画メッセージ</p>
                    <p class="main-magazine--item-des">ピアニスト 上原彩子氏よりメッセージ＃626トパーズ</p>
                  </div>
                </div>
              </a>
            </div>
          </div>
          <div class="align-center sp-only"><a href="" class="main-news--viewmore">一覧を見る</a></div>
        </div>
      </div><!-- ./main-magazine -->
      <div class="main-supporter">
        <h2 class="main-ttl">Supporter<span>支援企業</span></h2>
        <div class="main-supporter--special">
          <p class="main-supporter--ttl">特別支援企業</p>
          <div class="main-supporter--list">
            <a href="" class="main-supporter--item">
              <img src="<?php echo $PATH;?>/assets/images/top/supporter01.png" alt="">
            </a>
            <a href="" class="main-supporter--item">
              <img src="<?php echo $PATH;?>/assets/images/top/supporter02.png" alt="">
            </a>
            <a href="" class="main-supporter--item">
              <img src="<?php echo $PATH;?>/assets/images/top/supporter03.png" alt="">
            </a>
            <a href="" class="main-supporter--item">
              <img src="<?php echo $PATH;?>/assets/images/top/supporter04.png" alt="">
            </a>
            <a href="" class="main-supporter--item">
              <img src="<?php echo $PATH;?>/assets/images/top/supporter05.png" alt="">
            </a>
            <a href="" class="main-supporter--item">
              <img src="<?php echo $PATH;?>/assets/images/top/supporter06.png" alt="">
            </a>
            <a href="" class="main-supporter--item">
              <img src="<?php echo $PATH;?>/assets/images/top/supporter01.png" alt="">
            </a>
          </div>
        </div>
        <div class="main-supporter--special">
          <p class="main-supporter--ttl">特別支援団体</p>
          <div class="main-supporter--list center">
            <a href="" class="main-supporter--item type2">
              <img src="<?php echo $PATH;?>/assets/images/top/supporter-last.png" alt="">
            </a>
          </div>
        </div>
      </div><!-- ./main-supporter -->
    </div><!-- ./main-content -->
  </div><!-- ./main-inner -->
  <div class="main-banner">
    <div class="main-banner--list">
      <a href="" class="main-banner--item">
        <img src="<?php echo $PATH;?>/assets/images/top/banner01.jpg" alt="">
      </a>
      <a href="" class="main-banner--item">
        <img src="<?php echo $PATH;?>/assets/images/top/banner02.jpg" alt="">
      </a>
      <a href="" class="main-banner--item">
        <img src="<?php echo $PATH;?>/assets/images/top/banner03.jpg" alt="">
      </a>
    </div>
  </div><!-- ./main-banner -->
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>